package com.example.wesley.fatordepotencia;

// Importação de alguma bibliotecas padrões para o desenvolvimento mobile

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

// Classe responsavel pelas funcões de entrada e saída de dados no aplicativo

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // variavel axiliar para verificar se o sistema é mono ou trifasico

    private int val;

    /*
     instaciação das classes usadas:
     ViewHolder = Classe usada na abstração da captura dos dados inputados pelo usuario
    */
    private ViewHolder mViewHolder = new ViewHolder();

    // Formulas = Classe usada para abstração da formula de calculo do fator de potencia

    private Formulas mFormoulas = new Formulas();

    // Metodo padão usado na criação da tela refenrente a esta classe

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mViewHolder.editValue1 = (EditText) findViewById(R.id.edit_value1);
        this.mViewHolder.editValue2 = (EditText) findViewById(R.id.edit_value2);
        this.mViewHolder.editValue3 = (EditText) findViewById(R.id.edit_value3);
        this.mViewHolder.editValue4 = (EditText) findViewById(R.id.edit_value4);
        this.mViewHolder.textResultado = findViewById(R.id.text_resultado);
        this.mViewHolder.buttonCalculate = (Button) findViewById(R.id.button_calculate);
        this.mViewHolder.radioMono = findViewById(R.id.radio_mono);
        this.mViewHolder.radioTrifa = findViewById(R.id.radio_trifa);
        this.mViewHolder.buttonCalculate.setOnClickListener(this);
    }

    /*
       Metodo usado para observar os radio buttons: 'monofasico' e 'trifasico'
       No momento em que um dos valores forem celecionados o metodo será acionado
       e irá salvar qual das opções está selecionada.
    */
    public void onRadioButtonClicked(View v){
        boolean checked = ((RadioButton) v).isChecked();

        switch(v.getId()) {
            case R.id.radio_mono:
                if (checked)
                    val = 0;
                    break;
            case R.id.radio_trifa:
                if (checked)
                    val = 1;
                    break;
        }
    }

    /*
        Metodo que observa o clicar do botão principal 'CALCULAR', onde ao ser clicado
        O metodo preencherá as variaveis usadas na classe 'Formulas' para realisar o calculo.
    */

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.button_calculate){

            //ff = Fator de potencia atual
            double ff = Double.valueOf(this.mViewHolder.editValue1.getText().toString());

            //ptcn = potencia total consumida
            double ptcn = Double.valueOf(this.mViewHolder.editValue2.getText().toString());

            //ffd =  Fator de potencia desejada
            double ffd = Double.valueOf(this.mViewHolder.editValue3.getText().toString());

            //f = frequencia
            double f = 60;

            //tsao = tensão
            double tsao = Double.valueOf(this.mViewHolder.editValue4.getText().toString());


            //
            switch (val) {
                case 0:
                    break;
                case 1:
                    ptcn = ptcn/3;
                    break;
            }

            //Funções usadas no calculo de fator de potencia
            this.mFormoulas.CalcPotat(ff, ptcn);
            this.mFormoulas.CalcPotdz(ffd, ptcn);
            this.mFormoulas.CalcReacp(tsao, (this.mFormoulas.Qd - this.mFormoulas.Qa));
            this.mFormoulas.CalcCapQ(f);

            //Função da classe ViewHolder para mostrar o resultado na tela.
            this.mViewHolder.textResultado.setText(String.format("A capacitância é de %.3fmF", this.mFormoulas.ceQ*1000));
        }
    }


    // Classe ViewHolder e suas variaves para a abstração da captura dos dados do usuario
    private static class ViewHolder {
        EditText editValue1;
        EditText editValue2;
        EditText editValue3;
        EditText editValue4;
        TextView textResultado;
        Button buttonCalculate;
        RadioButton radioMono;
        RadioButton radioTrifa;
    }


    // Classe formulas e suas funções para o calculo do fator de pontecia
    private static class Formulas {

        // Potencia reativa desejada
        double Qd;

        // Potencia reativa atual
        double Qa;

        // reatancia capacitiva
        double Xcap;

        // capacitância equivalente
        double ceQ;

        // potencia reativa atual; variaveis: ff = Fator de potencia atual, ptat = potencia consumda
        public double CalcPotat(double ff, double ptcn){
            return Qa = ((ptcn*1000)*(Math.tan(Math.acos(ff))));
        }
        // potencia reativa de desejada
        public double CalcPotdz(double ff, double ptcn){
            return Qd = ((ptcn*1000)*(Math.tan(Math.acos(ff))));
        }
        // reatancia capacitiva; Variaveis: tsao = tensão
        public double CalcReacp(double tsao, double Qcap){
            return Xcap = (-(Math.pow(tsao,2))/Qcap);
        }
        // Capacitância equivalente; variaveis: f = frequencia
        public double CalcCapQ(double f){
            return ceQ = (1/(2*Math.PI*f*Xcap));
        }
    }
}

